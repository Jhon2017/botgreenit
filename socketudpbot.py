import socket
from bot import response
# Create a UDP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
# Bind the socket to the port
server_address = ('localhost', 10000)
sock.bind(server_address)
while True:
    data, address = sock.recvfrom(512)
    if data:
        res = response(data.decode("utf-8"))
        if res is not None:
        	sock.sendto(res.encode("utf-8"), address)
